package bandymaisulokalemis;

import java.text.ChoiceFormat;
import java.text.MessageFormat;

public class Bandymas {

    public static void main(String[] args) {


        double[] limits = {0, 1, 2, 10, 21, 22, 30, 31, 32};
        String[] part = {
                "nulis obuolių",
                "vienas obuolys",
                "{0} obuoliai",
                "{0} obuolių",
                "{0} obuolys",
                "{0} obuoliai",
                "{0} obuolių",
                "{0} obuolys",
                "{0} obuoliai"
        };
        ChoiceFormat choiceFormat = new ChoiceFormat(limits, part);

       String message = "Ant {1} guli {0}";

        MessageFormat messageFormat = new MessageFormat(message);
       messageFormat.setFormatByArgumentIndex(0, choiceFormat);

        for (int i = 0; i < 5; i++) {

            String result = messageFormat.format(new Object[] {i, "stalo"});
            System.out.println(result);

        }

        System.out.println(messageFormat.format(new Object[] { 20, "stalo" }));
        System.out.println(messageFormat.format(new Object[] { 21, "stalo" }));
        System.out.println(messageFormat.format(new Object[] { 29, "stalo" }));
        System.out.println(messageFormat.format(new Object[] { 30, "stalo" }));
    }
}
