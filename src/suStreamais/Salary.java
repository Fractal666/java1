package suStreamais;

public class Salary {
    Employee darbuotojas;
    double alga;


    public Salary(Employee darbuotojas, double alga) {
        this.darbuotojas = darbuotojas;
        this.alga = alga;
    }

    public String getVarda() {
        return darbuotojas.getVardas();
    }



    @Override
    public String toString() {
        return "Salary{" +
                "darbuotojas=" + darbuotojas +
                ", alga=" + alga +
                '}';
    }
}
