package suStreamais;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        List<Salary> algos = new ArrayList<>();

        algos.add(new Salary(new Employee ("Rimas", "Kevalas"),750.00));
        algos.add(new Salary(new Employee ("Angele", "Raudonikis"),500.00));
        algos.add(new Salary(new Employee ("Kindredas", "Serksnas"),325.00));
        algos.add(new Salary(new Employee ("Vejas", "Nupustas"),1500.00));
        algos.add(new Salary(new Employee ("Svetlana", "Liuks"),1250.00));
        algos.add(new Salary(new Employee ("Svetlana", "Liuks"),150.00));

        List<Salary> sorted=algos.stream().sorted((x,y)->Double.compare(y.alga, x.alga)).collect(Collectors.toList());

        for(Salary x:sorted){
            System.out.println(x);
        }

        Map<String, Long> kiekis = algos.stream().collect(Collectors.groupingBy(x -> x.darbuotojas.getVardas(), Collectors.counting()));

        System.out.println(kiekis);




    }
double sum;
    public void draw(double f) {
       sum= f*f;
    }

}
