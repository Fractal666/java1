package turnyras;

public class Pora {
    Dalyvis dalyvis1;
    Dalyvis dalyvis2;

    Pora(Dalyvis dalyvis1, Dalyvis dalyvis2) {

        this.dalyvis1=dalyvis1;
        this.dalyvis2=dalyvis2;
    }

    public Dalyvis randomas(){
        double random= Math.random();
        if(random<0.5){
         return   this.dalyvis1;

        }else{
            return this.dalyvis2;
        }
    }
}
