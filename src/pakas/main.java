package pakas;

import java.util.*;
import java.util.stream.Collectors;

public class main {



    public static void main(String[] args) {
Preke[] prekes1= new Preke[]{
        new Preke("0001", "Kojines", 2.00, 1, 10, 1.89, true),
        new Preke("0002", "Kede", 15.00, 4, 15, 9.00, true),
        new Preke("0003", "Duona", 1.00, 1, 40, 0.40, false),
        new Preke("0004", "Flomasteriai", 4.50, 1, 5, 3.19, false)

};


        Preke[] prekes2={
                new Preke("0001","Kojines",2.00, 1, 10, 1.80 ,true),
                new Preke("0002","Kede",15.00, 1, 15, 9.00 ,true),
                new Preke("0003","Duona",1.00, 10, 40, 0.40 ,false),
                new Preke("0004","Flomasteriai",4.50, 4, 5, 3.19 ,false)
        };

        Preke[] prekes3={
                new Preke("0001","Kojines",2.00, 15, 10, 1.85 ,true),
                new Preke("0002","Kede",15.00, 7, 15, 9.99 ,true),
                new Preke("0003","Duona",1.00, 12, 40, 0.45 ,false),
                new Preke("0004","Flomasteriai",4.50, 16, 5, 3.10 ,false)
        };








        List<Klientas> listasKlientu = new ArrayList<>();

        listasKlientu.add ( new Klientas(new Date(System.currentTimeMillis()), "1111", "Petras", prekes1));
        listasKlientu.add(new Klientas(new Date(System.currentTimeMillis()), "1111", "Justas", prekes2));
        listasKlientu.add(new Klientas(new Date(System.currentTimeMillis()), "1111", "Katazyna 'Keit22' Balcevic", prekes3));



List<Klientas> kazkas=listasKlientu.stream().sorted((k1, k2)->Double.compare(k1.uzdarbis, k2.uzdarbis)).limit(2).collect(Collectors.toList());

        for(Klientas x: kazkas){
System.out.println(x);}

//        Optional<Klientas> vidurkis = listasKlientu.stream()
//                               .map(x -> new Klientas(x.uzdarbis(), 1))
//                               .reduce((a, b) -> new Klientas(
//                                                a.plotas + b.plotas,
//                                                a.skaicius+b.skaicius));
//                System.out.println("Vidurkis=" +
//                                (vidurkis.isPresent() ? vidurkis.get().vidurkis() : "nera") );
//
//
//                            }

        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        Map<String, Integer> kazkas2 = listasKlientu.stream().flatMap(x->Arrays.stream(x.getPrekes())).collect(Collectors.groupingBy(Preke::getPavadinimas, Collectors.summingInt(Preke::getKiekis))) ;

        System.out.println("Daugiausia buvo nupirkta pagal kieki: " +kazkas2.entrySet().stream().sorted((o1,o2)->o2.getValue()-o1.getValue()).limit(2).collect(Collectors.toList()));

            //System.out.println(kazkas2);

        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

            Map<String, Long> kazkas5 = listasKlientu.stream().flatMap(x->Arrays.stream(x.getPrekes())).collect(Collectors.groupingBy(Preke::getPavadinimas, Collectors.counting()));
            System.out.println("Daugiausia buvo atlikta pirkimu: "+kazkas5.entrySet().stream().sorted((o1,o2)->Long.compare(o2.getValue(), o1.getValue())).limit(2).collect(Collectors.toList()));
//System.out.println(kazkas5);

        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        Map<String, Double> kazkas10 = listasKlientu.stream().flatMap(x->Arrays.stream(x.getPrekes())).collect(Collectors.groupingBy(Preke::getPavadinimas, Collectors.summingDouble((x->x.proc())))) ;

        System.out.println("Prekes uzdarbis: "+kazkas10.entrySet().stream().sorted((o1,o2)->Double.compare(o2.getValue(), o1.getValue())).limit(2).collect(Collectors.toList()));







      List<Preke> kazkas12 = listasKlientu.stream().flatMap(x->Arrays.stream(x.getPrekes())).sorted((k1, k2)->Double.compare(k1.proc(), k2.proc())).limit(2).collect(Collectors.toList());;


      for(Preke x: kazkas12){
           System.out.println(x);}

    }

}
