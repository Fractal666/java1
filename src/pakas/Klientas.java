package pakas;

import java.util.Arrays;
import java.util.Date;

public class Klientas {

    private Date dokumentoData;
    private String dokumentoNumeris;
    private String klientoPavadinimas;
    private Preke[] prekes;
    double uzdarbis;


    public Klientas(Date dokumentoData, String dokumentoNumeris, String klientoPavadinimas, Preke[] prekes) {
        this.dokumentoData = dokumentoData;
        this.dokumentoNumeris = dokumentoNumeris;
        this.klientoPavadinimas = klientoPavadinimas;
        this.prekes = prekes;
        this.uzdarbis=uzdarbis();
    }

    @Override
    public String toString() {
        return  klientoPavadinimas + '\'' +
                ", uzdarbis=" + uzdarbis +
                '}';
    }



    public Date getDokumentoData() {
        return dokumentoData;
    }

    public void setDokumentoData(Date dokumentoData) {
        this.dokumentoData = dokumentoData;
    }

    public String getDokumentoNumeris() {
        return dokumentoNumeris;
    }

    public void setDokumentoNumeris(String dokumentoNumeris) {
        this.dokumentoNumeris = dokumentoNumeris;
    }

    public String getKlientoPavadinimas() {
        return klientoPavadinimas;
    }

    public void setKlientoPavadinimas(String klientoPavadinimas) {
        this.klientoPavadinimas = klientoPavadinimas;
    }

    public Preke[] getPrekes() {
        return prekes;
    }



    public void setPrekes(Preke[] prekes) {
        this.prekes = prekes;
    }


    public static void getMax(Klientas[] masyvas) {
        double max = 0;
        Klientas k = null;
        for (int i = 0; i < masyvas.length; i++) {
            if (masyvas[i].uzdarbis() > max) {
                max = masyvas[i].uzdarbis();
                k = masyvas[i];
            }

        }
        System.out.println("Klientas " + k.klientoPavadinimas + " uzdirbo parduotuvei daugiausiai - " + k.uzdarbis() + " euru.");
    }

    public double uzdarbis() {
        double suma = 0;
        for (int i = 0; i < prekes.length; i++) {
            suma += prekes[i].getKainaBePvm() * prekes[i].getKiekis() * (1 - (prekes[i].getNuolaida() / 100)) - prekes[i].getSavikaina() * prekes[i].getKiekis();
        }
        return (double) Math.round(suma * 100) / 100;
    }





    }