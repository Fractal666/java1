package generics;

public class Zmogus {
    private String vardas;

    public Zmogus(String vardas) {
        this.vardas = vardas;
    }

    public String getVardas() {
        return vardas;
    }

    @Override
    public String toString() {
        return
                "vardas='" + vardas;
    }
}
