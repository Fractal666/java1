package generics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {

    public static void main(String[] args) {



      Automobilis auto1=new Automobilis("Audi", "123");
        Automobilis auto2=new Automobilis("Volkswagen", "123");
        Automobilis auto3=new Automobilis("BMW", "123");
        Automobilis auto4=new Automobilis("Mercedes", "123");

        Saugykla<Automobilis> listas = new Saugykla<>();


        listas.add(auto1);
        listas.add(auto2);
        listas.add(auto3);
        listas.add(auto4);

        for(Automobilis a : listas.getSaugykla()){
            System.out.println(a.getMarke()+ " numeris: " + a.getNumeris());
        }

        System.out.println("\n po istrynimo: \n");

        listas.remove(auto4);

        for(Automobilis a : listas.getSaugykla()){
            System.out.println(a.getMarke()+ " numeris: " + a.getNumeris());
        }

        System.out.println("\n");


        Iterator<Automobilis> iter = listas.iterator();

        while(iter.hasNext()){
            Automobilis ab = iter.next();
            System.out.println(ab.getMarke());
        }
        System.out.println("\n");


        iter = listas.iteratorRev();

   while(iter.hasNext()){
     Automobilis ab = iter.next();
      System.out.println(ab.getMarke());
    }

        for (Iterator<Automobilis> it = listas.iteratorRev(); it.hasNext(); ) {
            Automobilis a = it.next();
            System.out.println(a.getMarke()+ " numeris: " + a.getNumeris());
        }
    }
}
