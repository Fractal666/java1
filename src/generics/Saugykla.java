package generics;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;


public class Saugykla<E> implements Iterable<E>  {
    private ArrayList<E> saugykla;

public Saugykla(){
    this.saugykla=new ArrayList<>();
}
    public ArrayList<E> getSaugykla() {
        return saugykla;
    }

    public void add(E item){
    this.saugykla.add(item);
    }

    public void remove(E item){
        this.saugykla.remove(item);
    }
    @Override
    public Iterator<E> iterator() {
        return this.saugykla.iterator();
    }

    public Iterator<E> iteratorRev(){
        return new IteratorRev(saugykla);
    }

    class IteratorRev implements Iterator<E>{


        ArrayList<E> list;
        int current;

        public IteratorRev(ArrayList<E> list) {
            this.list = list;
            this.current = list.size();
        }

        @Override
        public boolean hasNext() {
            return current >0;
        }

        @Override
        public E next() {
            return list.get(--current);
        }
    }
}
