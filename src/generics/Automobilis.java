package generics;

public class Automobilis  {
    private String marke;
    private String numeris;


    public Automobilis(String marke, String numeris) {
        this.marke = marke;
        this.numeris = numeris;
    }

    @Override
    public String toString() {
        return
                "marke=" + marke +
                ", numeris=" + numeris;

    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public String getNumeris() {
        return numeris;
    }

    public void setNumeris(String numeris) {
        this.numeris = numeris;
    }
}
