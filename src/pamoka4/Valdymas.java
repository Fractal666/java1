package pamoka4;

import java.util.Arrays;
import java.util.Comparator;

public class  Valdymas {

    public static void main(String[] args) {

        Mokinys[] mokiniai = {
                new Mokinys("Lukas", "Pavardenis", new int[]{8, 9, 8, 10}),
                new Mokinys("Lukas2", "Pavardenis2", new int[]{5, 6, 7, 5}),
                new Mokinys("Lukas3", "Pavardenis3", new int[]{5, 6, 7, 7}),
                new Mokinys("Lukas4", "Pavardenis4", new int[]{5, 6, 7, 6})
        };
        System.out.println(mokiniai[0].vardas);
/*for(int i=0; i<mokiniai.length; i++){
        System.out.println(mokiniai[i].vardas + " " + mokiniai[i].pavarde + " " + "Trimestras: " + mokiniai[i].average());
}*/
        Comparator<Mokinys> lyginimas=Comparator.comparing(Mokinys::getVidurki);

        Arrays.sort(mokiniai, lyginimas.reversed());
        for(Mokinys x : mokiniai){
            System.out.println(x.vardas+" "+ x.pavarde+ " " +"Trimestras: "+ x.average());
        }
    }
    }
