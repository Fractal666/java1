package prekes;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

public class main {



    public static void main(String[] args) {
Preke[] prekes1={
        new Preke("0001","Kojines",2.00, 1, 10, 1.89 ,true),
        new Preke("0002","Kede",15.00, 4, 15, 9.00 ,true),
        new Preke("0003","Duona",1.00, 1, 40, 0.40 ,false),
        new Preke("0004","Flomasteriai",4.50, 1, 5, 3.19 ,false)
        };
//Isspausdina visas prekes
/*for(Preke x:prekes1){
    System.out.println(x);
}*/

        Preke[] prekes2={
                new Preke("0001","Kojines",2.00, 1, 10, 1.80 ,true),
                new Preke("0002","Kede",15.00, 1, 15, 9.00 ,true),
                new Preke("0003","Duona",1.00, 10, 40, 0.40 ,false),
                new Preke("0004","Flomasteriai",4.50, 4, 5, 3.19 ,false)
        };

        Preke[] prekes3={
                new Preke("0001","Kojines",2.00, 15, 10, 1.85 ,true),
                new Preke("0002","Kede",15.00, 7, 15, 9.99 ,true),
                new Preke("0003","Duona",1.00, 12, 40, 0.45 ,false),
                new Preke("0004","Flomasteriai",4.50, 16, 5, 3.10 ,false)
        };

        Klientas[] klientai={

                new Klientas(new Date(System.currentTimeMillis()), "1111", "Petras", prekes1),
                new Klientas(new Date(System.currentTimeMillis()), "1111", "Justas", prekes2),
                new Klientas(new Date(System.currentTimeMillis()), "1111", "Katazyna 'Keit22' Balcevic", prekes3)
        };
//Isspausdina klientus ir ju uzdirbta suma parduotuvei
       /* for(Klientas x:klientai){
            System.out.println(x.getKlientoPavadinimas()+" "+x.uzdarbis());
        }*/

        Klientas.getMax(klientai);

        Comparator<Klientas> lyginimas=Comparator.comparing(Klientas::uzdarbis);

        Arrays.sort(klientai, lyginimas.reversed());

        System.out.println("Klientai uzdirbe daugiausia pelno parduotuvei: ");
        for(int i=0; i<2; i++){
            System.out.println(klientai[i].getKlientoPavadinimas()+" "+klientai[i].uzdarbis());
        }

        for(Klientas x:klientai){
      //Spausdina pirmu dvieju isortintu klientu uzdirbta suma parduotuvei    //  System.out.println(x.getKlientoPavadinimas()+" "+x.uzdarbis());
            if(x.getMaxPrekiuKieki()== Klientas.getMaxKieki(klientai, x.getPrekes())){
                System.out.println(x.getKlientoPavadinimas()+" - Didziausias prekes '"+  x.gautiPreke(x.getPrekes())  +"' kiekis: "+Klientas.getMaxKieki(klientai, x.getPrekes()));
            }

        }





    }

}
