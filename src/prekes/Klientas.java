package prekes;

import java.util.Arrays;
import java.util.Date;

public class Klientas {

    private Date dokumentoData;
    private String dokumentoNumeris;
    private String klientoPavadinimas;
    private Preke[] prekes;


    public Klientas(Date dokumentoData, String dokumentoNumeris, String klientoPavadinimas, Preke[] prekes) {
        this.dokumentoData = dokumentoData;
        this.dokumentoNumeris = dokumentoNumeris;
        this.klientoPavadinimas = klientoPavadinimas;
        this.prekes = prekes;
    }

    @Override
    public String toString() {
        return "Klientas{" +
                "dokumentoData=" + dokumentoData +
                ", dokumentoNumeris='" + dokumentoNumeris + '\'' +
                ", klientoPavadinimas='" + klientoPavadinimas + '\'' +
                ", prekes=" + Arrays.toString(prekes) +
                '}';
    }

    public double uzdarbis() {
        double suma = 0;
        for (int i = 0; i < prekes.length; i++) {
            suma += prekes[i].getKainaBePvm() * prekes[i].getKiekis() * (1 - (prekes[i].getNuolaida() / 100)) - prekes[i].getSavikaina() * prekes[i].getKiekis();
        }
        return (double) Math.round(suma * 100) / 100;
    }

    public Date getDokumentoData() {
        return dokumentoData;
    }

    public void setDokumentoData(Date dokumentoData) {
        this.dokumentoData = dokumentoData;
    }

    public String getDokumentoNumeris() {
        return dokumentoNumeris;
    }

    public void setDokumentoNumeris(String dokumentoNumeris) {
        this.dokumentoNumeris = dokumentoNumeris;
    }

    public String getKlientoPavadinimas() {
        return klientoPavadinimas;
    }

    public void setKlientoPavadinimas(String klientoPavadinimas) {
        this.klientoPavadinimas = klientoPavadinimas;
    }

    public Preke[] getPrekes() {
        return prekes;
    }

    public int getMaxPrekiuKieki(){
        int max=0;
        for(Preke p:prekes){
            if(p.getKiekis()> max)
                max = p.getKiekis();
        }
        return max;
    }

    public void setPrekes(Preke[] prekes) {
        this.prekes = prekes;
    }


    public static void getMax(Klientas[] masyvas) {
        double max = 0;
        Klientas k = null;
        for (int i = 0; i < masyvas.length; i++) {
            if (masyvas[i].uzdarbis() > max) {
                max = masyvas[i].uzdarbis();
                k = masyvas[i];
            }

        }
        System.out.println("Klientas " + k.klientoPavadinimas + " uzdirbo parduotuvei daugiausiai - " + k.uzdarbis() + " euru.");
    }

    public static int getMaxKieki(Klientas[] masyvas, Preke[] prekes) {
        int maxKiekis = 0;
        int ki = 0;
        for (int i = 0; i < masyvas.length; i++) {
            for (int j = 0; j < prekes.length; j++) {

                if (masyvas[i].prekes[j].getKiekis() > maxKiekis) {
                    maxKiekis = masyvas[i].prekes[j].getKiekis();
                    ki = masyvas[i].prekes[j].getKiekis();
                }
            }


        }

        return ki;
    }

   public static String gautiPreke(Preke[] prekes) {
String b ="";
        for (int i = 0; i < prekes.length; i++) {
           b=  prekes[i].getPavadinimas();
        }
        return  b;
    }
}