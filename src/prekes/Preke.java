package prekes;

public class Preke {

    private String prekesKodas;
    private String pavadinimas;
    private Double kainaBePvm;
    private Integer kiekis;
    private Integer nuolaida;
    private Double savikaina;
    private int pvm;

    public void setPvm(Boolean x){
        this.pvm=(x==true)?21:9;
    }

    public Preke(String prekesKodas, String pavadinimas, double kainaBePvm, int kiekis, int nuolaida, double savikaina, boolean pvm) {
        this.prekesKodas = prekesKodas;
        this.pavadinimas = pavadinimas;
        this.kainaBePvm = kainaBePvm;
        this.kiekis = kiekis;
        this.nuolaida = nuolaida;
        this.savikaina = savikaina;
        setPvm(pvm);
    }




    public int getNuolaida() {
        return nuolaida;
    }

    public void setNuolaida(int nuolaida) {
        this.nuolaida = nuolaida;
    }

    public double getSavikaina() {
        return savikaina;
    }

    public void setSavikaina(double savikaina) {
        this.savikaina = savikaina;
    }

    public String getPrekesKodas() {

        return prekesKodas;
    }

    public void setPrekesKodas(String prekesKodas) {
        this.prekesKodas = prekesKodas;
    }

    public String getPavadinimas() {
        return pavadinimas;
    }

    public void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }



    public double getKainaBePvm() {
        return kainaBePvm;
    }

    public void setKainaBePvm(double kainaBePvm) {
        this.kainaBePvm = kainaBePvm;
    }

    public int getKiekis() {
        return kiekis;
    }

    public void setKiekis(int kiekis) {
        this.kiekis = kiekis;
    }

    @Override
    public String toString() {
        return "Preke{" +
                "prekesKodas='" + prekesKodas + '\'' +
                ", pavadinimas='" + pavadinimas + '\'' +
                ", kainaBePvm=" + kainaBePvm +
                ", kiekis=" + kiekis +
                ", nuolaida=" + nuolaida +
                ", savikaina=" + savikaina +
                ", pvm=" + pvm +
                '}';
    }




}
