package automobiliai2;

import java.util.Comparator;

public class Automobilis {
    String numeriai;
    Savininkas savininkas;

    public Automobilis(String numeriai, Savininkas savininkas) {
        this.numeriai = numeriai;
        this.savininkas = savininkas;
    }

    public String getNumeriai() {
        return numeriai;
    }

    public void setNumeriai(String numeriai) {
        this.numeriai = numeriai;
    }

    public Savininkas getSavininkas() {
        return savininkas;
    }

    public void setSavininkas(Savininkas savininkas) {
        this.savininkas = savininkas;
    }

    @Override
    public String toString() {
        return "Automobilis{" +
                "numeriai='" + numeriai + '\'' +
                ", savininkas=" + savininkas +
                '}';
    }

    public static class Savininkas{
        String vardas;
        String pavarde;

        public Savininkas(String vardas, String pavarde) {
            this.vardas = vardas;
            this.pavarde = pavarde;
        }

        public String getVardas() {
            return vardas;
        }

        public void setVardas(String vardas) {
            this.vardas = vardas;
        }

        public String getPavarde() {
            return pavarde;
        }

        public void setPavarde(String pavarde) {
            this.pavarde = pavarde;
        }

        @Override
        public String toString() {
            return "Savininkas{" +
                    "vardas='" + vardas + '\'' +
                    ", pavarde='" + pavarde + '\'' +
                    '}';
        }
    }

    public static Comparator<Automobilis> pagalPavardeLyginimas = new Comparator<Automobilis>() {
        @Override
        public int compare(Automobilis o1, Automobilis o2) {
            String pagalPavarde1 = o1.getSavininkas().getPavarde();
            String pagalPavarde2 = o2.getSavininkas().getPavarde();
            return pagalPavarde1.compareTo(pagalPavarde2);
        }
    };

    public static Comparator<Automobilis> pagalVardasLyginimas = new Comparator<Automobilis>() {
        @Override
        public int compare(Automobilis o1, Automobilis o2) {
            String pagalVardas1 = o1.getSavininkas().getVardas();
            String pagalVardas2 = o2.getSavininkas().getVardas();
            return pagalVardas1.compareTo(pagalVardas2);
        }
    };

    public static Comparator<Automobilis> pagalNumeriaiLyginimas = new Comparator<Automobilis>() {
        @Override
        public int compare(Automobilis o1, Automobilis o2) {
            String pagalNumeriai1 = o1.getNumeriai();
            String pagalNumeriai2 = o2.getNumeriai();
            return pagalNumeriai1.compareTo(pagalNumeriai2);
        }
    };
}
