package automobiliai2;

import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {

        Automobilis auto1 = new Automobilis("ABC-123",  new Automobilis.Savininkas("Petras", "Jasinskas"));
        Automobilis auto2 = new Automobilis("CDE-458",  new Automobilis.Savininkas("Darius", "Navickas"));
        Automobilis auto3 = new Automobilis("A8745BC",  new Automobilis.Savininkas("Justas", "Karaliunas"));
        Automobilis auto4 = new Automobilis("A-k1L258",  new Automobilis.Savininkas("Modestas", "Mikelionis"));

        ArrayList<Automobilis> masinos = new ArrayList<Automobilis>();

        masinos.add(auto1);
        masinos.add(auto2);
        masinos.add(auto3);
        masinos.add(auto4);


        // PAGAL PAVARDE
        System.out.println("Pagal pavarde: ");
        Collections.sort(masinos, Automobilis.pagalPavardeLyginimas);

        for(Automobilis x: masinos){
            System.out.println(x);
        }

        // PAGAL VARDA
        System.out.println("Pagal varda: ");
        Collections.sort(masinos, Automobilis.pagalVardasLyginimas);

        for(Automobilis x: masinos){
            System.out.println(x);
        }

        // PAGAL NUMERIUS
        System.out.println("Pagal numerius: ");
        Collections.sort(masinos, Automobilis.pagalNumeriaiLyginimas);

        for(Automobilis x: masinos){
            System.out.println(x);
        }

    }
}
