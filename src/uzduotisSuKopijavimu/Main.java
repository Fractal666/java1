package uzduotisSuKopijavimu;

import java.io.*;

public class Main {


    public static void main(String[] args) {



            copyFiles("C:\\Users\\Lukas\\IdeaProjects\\Mokiniai\\src\\terminalas", "C:\\Users\\Lukas\\IdeaProjects\\Mokiniai\\src\\spausd");

        System.out.println(Main.class.getPackage().getName());


    }

    public static void copyFiles(String copyFrom, String copyTo) {


        File folder = new File(copyFrom);
        File[] files = folder.listFiles();
        File destFolder = new File(copyTo);

        if (!destFolder.exists()) {


            destFolder.mkdir();
        }

        for (File f : files) {
            if (f.isDirectory()) {
                System.out.println(destFolder.getPath().concat("\\" + f.getName()));
                File subFold = new File(destFolder.getPath().concat("\\" + f.getName()));
                subFold.mkdir();
                copyFiles(f.getPath(), subFold.getPath());
            }


            if (f.isFile()) {
                File fileCheck = new File(destFolder.getPath().concat("\\"+f.getName()));

                if(fileCheck.exists()){
                    fileCheck.delete();
                    System.out.println("file has been replaced");
                }
                try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(f.getPath()));
                     BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(destFolder.getPath().concat("\\" + f.getName())))) {
                    System.out.println(destFolder.getPath() + f.getName());
                    byte[] buffer = new byte[1024];
                    int obj;
                    while ((obj = in.read(buffer, 0, buffer.length)) != -1) {
                        out.write(buffer, 0, obj);
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("files copied succesfully!");
            }
        }
    }
}

