package namuDarbasSuMonetomis;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print("Iveskite suma: ");
        Scanner in = new Scanner(System.in);
        int input=in.nextInt();


        while(input!=0) {
            int result = metodas(input);
            System.out.println(result);
            input -= result;
        }
        }
    static int metodas(int input){
        int result=0;
        int i=0;
        while(result<=input){


            result=(int) Math.pow(2, ++i);
        }

        return (int)Math.pow(2, i-1);

    }
}
/*
//kitas pavizdys
    public static void main(String[] args) {
        kapeikos(144);//0x90=0b0000 0000 1001 0000
         kapeikos(144);//0x90=0b0000 0000 0001 0010
    }
    static void kapeikos(int suma){
    int a=suma & 0b11111111;
        System.out.println("suma="+a);
    }
    */