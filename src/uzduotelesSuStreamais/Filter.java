package uzduotelesSuStreamais;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Filter {
    public static void main(String[] args) {
        IntStream stream = IntStream.range(1, 101);
        // Srauto negalima panaudoti du kartus!!!
        // kadangi count() suskaičiuoja elementus "praleisdamas" srautą
        // tai tolimesni vaiksmai su tuo pačiu srautu iššauks klaidą
        //System.out.println("Viso " + stream.count());
        long count2 = stream
                .filter(x -> x % 2 == 0)
                .filter(x -> x > 10)
                .count();
        System.out.println("Viso lyginių didesnių už 10 yra " + count2);

        // Map
        Stream<Integer>number= Stream.of(1,2,3,4,5);
       List<Integer> suma = number
                .filter(x -> x % 2 == 0)
               .collect(Collectors.toList());
       suma.forEach( x -> System.out.println(x));


       // System.out.println("Suma " + suma);

        //cia for each yra void tai negali nurodyti kad list Integer
       /* number
                .filter(x -> x % 2 == 0)
                .forEach(x-> System.out.println(x));*/


        // Sorted
        System.out.print("sorted");
        stream = new Random().ints(10, 1, 1000);
        stream.sorted().forEach(x -> System.out.print(" " + x));
    }
}
