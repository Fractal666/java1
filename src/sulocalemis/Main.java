package sulocalemis;

import java.text.ChoiceFormat;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class Main {

    public static void main(String[] args) {

        messages();

        Locale.setDefault(Locale.forLanguageTag("lt"));
       messages();



    }

    private static void messages() {

        ResourceBundle pranesimai = ResourceBundle.getBundle(
                Main.class.getPackage().getName() + ".kalbos");

        String message = pranesimai.getString("message");

        String[] part = pranesimai.getString("thing").split(":");
        String[] limits1= pranesimai.getString("limit").split(":");

        double[] limits=new double[limits1.length];

        for(int i=0; i< limits.length; i++){
            limits[i]=Double.parseDouble(limits1[i]);
        }

        ChoiceFormat choiceFormat = new ChoiceFormat(limits, part);

        MessageFormat messageFormat = new MessageFormat(message);
        messageFormat.setFormatByArgumentIndex(0, choiceFormat);

        for (int i = 0; i < 21; i++) {

            String result = messageFormat.format(new Object[] {i});
            System.out.println(result);

        }
    }
}
