package uzdavinysVU;

import java.io.*;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Uzdavinys {




    public static void main(String[] args) {
        //  pirmaFunkcija();

        //  antraFunkcija();

        //  treciaFunkcija();
        ketvirtaFunkcija();
    }
    public static void pirmaFunkcija(){
        Scanner scanner  = new Scanner(System.in);
        System.out.println("iveskite teksta");
        int count=0;
        String tekstas;
        tekstas = scanner.next();

        tekstas = tekstas.replace("ab","");
        char[] raides = tekstas.toCharArray();
        for(char i : raides){
            if(i=='a'|| i=='A'){
                count++;

            }
        }
        System.out.println("jusu tekste yra:"+ count+" raides pries kurias nera raides b");
    }

    public static void antraFunkcija(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("iveskite teksta");
        String txt  = scanner.nextLine();
        // txt = txt.replaceAll("\\s", "");

        String[] symbols = txt.split("");

        Map<String,Long> countIt= Arrays.stream(symbols).collect(Collectors.groupingBy(x->x,Collectors.counting()));


        System.out.println("jusu teksto simboliai : " + countIt.entrySet().toString());

    }

    public static void treciaFunkcija() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("iveskite sveikaji skaiciu");
        int num1 = scanner.nextInt();
        System.out.println("iveskite antra sveikaji skaiciu");
        int num2 = scanner.nextInt();
        int n = 0;
        int count = 0;
        //check if number is prime

        if (num1 < num2) {
            n = num1;
        } else {
            n = num2;
        }
        for (int i = 1; i <= n; i++) {
            if (num1 % i == 0) {
                if (num2 % i == 0) {
                    count++;
                }
            }
        }
        if (count == 1) {
            System.out.println("skaiciai yra tarpusavyje pirminiai");
        } else {
            System.out.println("skaiciai tarpusavyje nera pirminiai");
        }
    }

    public static void ketvirtaFunkcija(){


        Map<String,List<Zmogus>> sezonai;


        try(
                BufferedReader in = new BufferedReader(new FileReader("src\\uzdavinysVU\\zmones.txt"))){

            StringBuilder build = new StringBuilder();
            String a;

            while((a=in.readLine())!=null){
                build.append(a);
                build.append('\n');

            }

            String[] b = build.toString().split("\n");



            sezonai =    Arrays.stream(b)
                    .skip(1)
                    .map(x->{
                        String[] t = x.split("\\s");

                        return new Zmogus(t[0],t[1],t[2]);
                    })
                    .collect(Collectors.groupingBy(x->{
                        String d = x.getData().split("-")[1];
                        int k = Integer.parseInt(d);
                        return (k>2 && k<6)? "Pavasario sezonas" :
                                (k>5 && k<9) ? "Vasaros sezonas " :
                                        (k>8 && k<12)? "Rudens sezonas" : "Ziemos sezonas";


                    },Collectors.toList()));

            System.out.println(sezonai.toString());


            try(Writer wr = new BufferedWriter(new FileWriter("src\\uzdavinysVU\\output.txt"))){
                for(String s : sezonai.keySet()){
                    wr.write(s);
                    ((BufferedWriter) wr).newLine();
                    wr.write(sezonai.get(s).toString());
                    ((BufferedWriter) wr).newLine();
                }


            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}

