package uzdavinysVU;

import java.util.Date;

public class Zmogus {
    String vardas;
    String pavarde;
    String data;

    public String getVardas() {
        return vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public String getData() {
        return data;
    }

    public Zmogus(String vardas, String pavarde, String data) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.data = data;
    }

    @Override
    public String toString() {
        return "Zmogus{" +
                "vardas='" + vardas + '\'' +
                ", pavarde='" + pavarde + '\'' +
                ", data='" + data + '\'' +
                '}'+'\n';
    }
}
