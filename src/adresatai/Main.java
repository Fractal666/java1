package adresatai;

public class Main {

    public static void main(String[] args) {

        Objektas[] dataObjects = new Objektas[3];


        Objektas dataObject = new Objektas();
        dataObject.count = 5;
        dataObjects[0] = dataObject;

        dataObject = new Objektas();
        dataObject.count = 7;
        dataObjects[1] = dataObject;

        dataObject = new Objektas();
        dataObject.count = 9;
        dataObjects[2] = dataObject;

        int sum = 0;

        for(int i=0; i < dataObjects.length; i++){
            sum = sum + dataObjects[i].count;
        }

        System.out.println("Sum: " + sum);


    }
}
