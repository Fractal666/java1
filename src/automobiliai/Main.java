package automobiliai;

import klaseKlaseje.Darbuotojas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import static java.util.Collections.*;

public class Main {

    public static void main(String[] args) {


        Automobilis auto1 = new Automobilis("ABC-123", "BMW", new Automobilis.Savininkas("Petras", "Jasinskas"));
        Automobilis auto2 = new Automobilis("CDE-458", "Maserati", new Automobilis.Savininkas("Darius", "Navickas"));
        Automobilis auto3 = new Automobilis("A8745BC", "Porsche", new Automobilis.Savininkas("Justas", "Karaliunas"));
        Automobilis auto4 = new Automobilis("A-k1L258", "Audi", new Automobilis.Savininkas("Modestas", "Mikelionis"));
        Automobilis auto5 = new Automobilis("A-k1asdhfg258", "Mustang", new Automobilis.Savininkas("Katažyna", "Balcevič"));

        ArrayList<Automobilis> masinos = new ArrayList<Automobilis>();

        masinos.add(auto1);

        masinos.add(auto2);
        masinos.add(auto3);
        masinos.add(auto4);
        masinos.add(auto5);



        Comparator<Automobilis> lyginimas = Comparator.comparing(Automobilis::getPavarde).thenComparing(Automobilis::getVardas).thenComparing(Automobilis::getMarke) ;

       Collections.sort(masinos,lyginimas);

        for (Automobilis x : masinos) {

            System.out.println(x);
        }

    }

}
