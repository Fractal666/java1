package automobiliai;

public class Automobilis {

    private String numeris;
    private String marke;
    private Savininkas savininkas;

    public  Automobilis(String numeris, String marke, Savininkas savininkas) {
        this.numeris = numeris;
        this.marke = marke;
        this.savininkas = savininkas;
    }

    public String getNumeris() {
        return numeris;
    }

    public void setNumeris(String numeris) {
        this.numeris = numeris;
    }

    public String getMarke() {
        return marke;
    }

public String getVardas(){
        return savininkas.getVardas();

}
    public String getPavarde(){
        return savininkas.getPavarde();

    }


    public void setMarke(String marke) {
        this.marke = marke;
    }

    public Savininkas getSavininkas() {
        return savininkas;
    }

    public void setSavininkas(Savininkas savininkas) {
        this.savininkas = savininkas;
    }

    @Override
    public String toString() {
        return "Automobilis{" +
                "numeris='" + numeris + '\'' +
                ", marke='" + marke + '\'' +
                ", savininkas=" + savininkas +
                '}';
    }

    public static class Savininkas{
       private String vardas;
       private String pavarde;

        @Override
        public String toString() {
            return "Savininkas{" +
                    "vardas='" + vardas + '\'' +
                    ", pavarde='" + pavarde + '\'' +
                    '}';
        }

        public Savininkas(String vardas, String pavarde) {
            this.vardas = vardas;
            this.pavarde = pavarde;
        }

        public String getVardas() {
            return vardas;
        }

        public void setVardas(String vardas) {
            this.vardas = vardas;
        }

        public String getPavarde() {
            return pavarde;
        }

        public void setPavarde(String pavarde) {
            this.pavarde = pavarde;
        }
    }
}
