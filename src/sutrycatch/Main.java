package sutrycatch;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String... args) {

        try {
            int a = calc(10, 0);
            System.out.println(a);
        } catch (ArithmeticException e) {
            System.out.println("Klaida su dalyba!!!"+ e.getMessage());
            e.printStackTrace();
        }

        try {
            List<Integer> b = new ArrayList<>();
            System.out.println(get(b, 1));
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Klaida su kolekcija!!! " + e.getMessage() + e.getLocalizedMessage());
            e.printStackTrace();
        }finally {
            System.out.println("bet šiaip tai viskas gerai :)");
        }

        System.out.println("Pabaiga");
    }

    static int calc(int a, int b) {
        return a / b;
    }

    static Integer get(List<Integer> list, int index) {
        return list.get(index);
    }
}
