package Bankas;

public class Klientas implements Mokejimas {

    private String pavadinimas;
    private String account;
    private double suma;
    double[] istorija=new double[10];
    static int kiekis=0;


    public Klientas(String pavadinimas, String account, double suma){
        this.pavadinimas=pavadinimas;
        this.account=account;
        this.suma=suma;
        kiekis++;
    }

    @Override
    public String account() {
        return account;
    }

    @Override
    public String owner() {
        return pavadinimas;
    }

    @Override
    public double suma() {
        return suma;
    }

    int trigger=0;

    public  void pay(double mokestis) {
        if(istorija[istorija.length-1]!=0){
            double[] naujasMasyvas= new double[trigger+10];
            istorija=naujasMasyvas;
            System.arraycopy(istorija, 0, naujasMasyvas, 0, istorija.length);
            istorija = naujasMasyvas;
        }

       istorija[trigger]=mokestis;
       trigger++;
    }

    public String klientoInfo(){ return pavadinimas+", padarė mokėjimų "+trigger; }





    public String toString() {
        return pavadinimas + ", " +account+ ", "+ suma ;
    }

    public String getPavadinimas() {
        return pavadinimas;
    }

    public double visuMokejimuSuma(){
        double sum = 0;
        for (int i=0; i<istorija.length; i++) sum+=istorija[i];
        return sum;
    }

    public int mokejimuVirsVidurkio(double vidurkis){
        int virsVidurkio = 0;
        for(int i=0; i<istorija.length; i++){
            if (istorija[i] > vidurkis*2) virsVidurkio += 1;
            else {}
        }
        return virsVidurkio;
    }
}

