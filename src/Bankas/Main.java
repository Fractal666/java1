package Bankas;

import figuros.Figura;

import java.util.Arrays;

public class Main {


    public static void main(String[] args) {

      /*  Mokejimas[] masyvas={

                new Darbuotojas("Paulius", "Belekas", "LT45455", 500.25),
                new Darbuotojas("Rimas", "Tevas", "LT45455", 125),
                new Klientas("UAB Pomidorai",  "LT45455", 25.75),
                new Klientas("AB Krastovaizdis",  "LT45455", 5500.25)

        };

        for(Mokejimas elem: masyvas) System.out.println(elem);
*/

        Klientas[] klientuMasyvas = {
                new Klientas("LT1000001", "UAB Orange Republic", 12000),
                new Klientas("LT1000002", "UAB Mango Republic", 24000),
                new Klientas("LT1000003", "UAB Watermelon Republic", 36000),
                new Klientas("LT1000004", "UAB Banana Republic", 48000),
        };

        //for(double elem: k1.mokejimuIstorija) System.out.println(elem);

        //duomenys testavimui
        klientuMasyvas[0].pay(300);
        klientuMasyvas[0].pay(500);
        klientuMasyvas[1].pay(500);
        klientuMasyvas[1].pay(500);
        klientuMasyvas[1].pay(500);
        klientuMasyvas[2].pay(1200);


        //sukuriam visu Klientu objektu triggeriu masyva
        Integer[] trigMasyvas = new Integer[Klientas.kiekis];
        for (int i = 0; i < trigMasyvas.length; i++) {
            trigMasyvas[i] = klientuMasyvas[i].trigger;
        }

        //rasti max tarp visu triggeriu
        Integer[] trigerKopija = trigMasyvas.clone(); //pagalbinis array, kuriame keiciame tvarka

        int max = 0;

        for (int i = 0; i < trigerKopija.length - 1; i++) {
            for (int j = i; j < trigerKopija.length; j++) {
                if (trigerKopija[j] > max) {
                    max = trigerKopija[i];
                    trigerKopija[i] = trigerKopija[j];
                    trigerKopija[j] = max;
                }
            }
        }

        int objektoNumeris = Arrays.asList(trigMasyvas).indexOf(3);

        System.out.println("Klientas padaręs daugiausiai mokėjimų yra: " + klientuMasyvas[objektoNumeris].klientoInfo());

        //randam suma visu darytu mokejimu ir kiek mokejimu skaiciu
        double sumaVisuKlientu = 0;
        int sumaVisuMokejimu = 0;
        for (int i = 0; i < klientuMasyvas.length; i++) {
            sumaVisuKlientu += klientuMasyvas[i].visuMokejimuSuma();
            sumaVisuMokejimu += klientuMasyvas[i].trigger;
        }

        //randam vidurki
        double mokejimoVidurkis = sumaVisuKlientu / sumaVisuMokejimu;

        //printinam klientus kuriu bent vienas mokejimas virsija visu mokejimu vidurki bent du kartus
        System.out.println("\nĮmonės, kuriose bent vienas mokėjimas viršijantis visų mokėjimų vidurkį bent du kartus:");
        for (int i = 0; i < klientuMasyvas.length; i++) {
            if (klientuMasyvas[i].mokejimuVirsVidurkio(mokejimoVidurkis) > 0) {
                System.out.println(klientuMasyvas[i].getPavadinimas());
            }
        }
        for(double x:klientuMasyvas[1].istorija) System.out.println(x);
    }
}
