package Bankas;

public interface Mokejimas {

    String account ();
    String owner();
    double suma();
    public  void pay(double mokestis);
}
