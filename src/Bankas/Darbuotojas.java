package Bankas;

public class Darbuotojas implements Mokejimas {

    private String vardas;
    private String pavarde;
    private String account;
    private double suma;


    public Darbuotojas(String vardas, String pavarde, String account, double suma){
        this.vardas=vardas;
        this.pavarde=pavarde;
        this.account=account;
        this.suma=suma;
    }

    @Override
    public String account() {
        return account;
    }

    @Override
    public String owner() {
        return vardas + pavarde;
    }

    @Override
    public double suma() {
        return suma;
    }

    @Override
    public void pay(double mokestis) {

    }

    public String toString() {
        return vardas + ", " +pavarde + ", "+account+ ", "+ suma ;
    }
}
