package figuros;

public class Apskritimas extends Figura {

    double r;

    public Apskritimas( double r) {

        this.r = r;
    }

    @Override
    public double Plotas() {
        return Math.PI * r * r;
    }

    @Override
    public double Perimetras() {
        return Math.PI * 2*r;
    }
    @Override
    public double skaiciavimas(double plotas){
        this.r=Math.sqrt(plotas/Math.PI);
        return Perimetras();

    }
}