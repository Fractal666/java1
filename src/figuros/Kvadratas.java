package figuros;

public class Kvadratas extends Figura {

    private double a;


    public Kvadratas( double a) {
        this.a = a;
    }


    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    @Override
    public double Plotas() {
        return a * a;
    }

    @Override
    public double Perimetras() {
        return 4 * a;
    }
    @Override
   public double skaiciavimas(double plotas){
        this.a=Math.sqrt(plotas);
        return Perimetras();

   }
}


