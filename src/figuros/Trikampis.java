package figuros;

public class Trikampis extends Figura {
    private double a;


    public Trikampis( double a) {
        this.a = a;

    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }



    @Override
    public double Plotas() {

        return (a*a*Math.sqrt(3))/4;
    }

    @Override
    public double Perimetras() {
        return 3*a;
    }
    @Override
    public double skaiciavimas(double plotas){
        this.a=Math.sqrt((plotas*4)/Math.sqrt(3));
        return Perimetras();

    }
}
