package differenttypeobjects;

public class Zodis extends Objektas {
    private String zodis;
    String labas="Labas";

    public Zodis(String zodis) {
        this.zodis = zodis;
    }

    public String getZodis() {
        return zodis;
    }

    public void setZodis(String zodis) {
        this.zodis = zodis;
    }

    @Override
    public String toString() {
        return  zodis;
    }

    @Override
    public Object daryti() {
        return labas;
    }
}
