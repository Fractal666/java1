package differenttypeobjects;

public class Skaicius extends Objektas{
    private Integer skaicius;
    Integer labas=55;

    public Skaicius(Integer skaicius) {
        this.skaicius = skaicius;
    }

    public Integer getSkaicius() {
        return skaicius;
    }

    public void setSkaicius(Integer skaicius) {
        this.skaicius = skaicius;
    }

    @Override
    public String toString() {
        return skaicius.toString();
    }

    @Override
    public Object daryti() {
        return labas;
    }
}
