package lambda;

public class Main {

    public static void main(String[] args) {

        Container<Employee> kontikas =new Container<>();

        kontikas.add(new Employee("Zigmas", "Tevas"));
        kontikas.add(new Employee("Pigmas", "Kevas"));
        kontikas.add(new Employee("Figmas", "Sevas"));
        kontikas.add(new Employee("Rigmas", "Mevas"));

        for( Employee x: kontikas.order((o1, o2)-> o2.getVardas().compareTo(o1.getVardas()))){
            System.out.println(x);

        }


    }
}
