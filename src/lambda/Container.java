package lambda;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Container<E> {

    List<E> listas;

    public Container() {
        this.listas = new ArrayList<>();
    }

    public void add(E e){
        listas.add(e);
    }

    public Iterable<E> order(Comparator<E> comp){
        List<E> list= new ArrayList<>(listas);
        list.sort(comp);
        return list;
    }



}
