package suFailais;

import java.io.Serializable;

public class Mokinys implements Serializable {

    private String vardas;
    private String pavarde;
    private String klase;

    public Mokinys(String vardas, String pavarde, String klase) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.klase = klase;
    }

    public String getVardas() {
        return vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public String getKlase() {
        return klase;
    }

    @Override
    public String toString() {
        return
                "vardas='" + vardas + '\'' +
                ", pavarde='" + pavarde + '\'' +
                ", klase='" + klase
               ;
    }
}
