package suFailais;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Mokinys> mokiniai =new ArrayList<>();

        mokiniai.add(new Mokinys("Lukas","Pavardenis", "1a"));
        mokiniai.add(new Mokinys("Tomas","Kazkas", "3c"));
        mokiniai.add(new Mokinys("Petras","Zmogus", "4a"));
        mokiniai.add(new Mokinys("Viktoras","Greblys", "5d"));
        mokiniai.add(new Mokinys("Agne","Liepaite", "1a"));



        writeNode();

        Node root = readNode();



    }

    static void write( List<Mokinys> mokiniai) {
        try (
                ObjectOutput ou = new ObjectOutputStream(
                        new BufferedOutputStream(
                                new FileOutputStream("C:/Users/Fractal/Documents/NetBeansProjects/JavaApplication/src/suFailais/sarasas.txt")))
        ) {
            ou.writeUTF("Abrakadabra");
            ou.writeObject(mokiniai);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static void read() {
        try (
                ObjectInput in = new ObjectInputStream(
                        new BufferedInputStream(
                                new FileInputStream("C:/Users/Fractal/Documents/NetBeansProjects/JavaApplication/src/suFailais/sarasas.txt")))
        ) {

            System.out.println(in.readUTF());

            Object object = in.readObject();
            if (object instanceof List) {
                List<Mokinys> list2 = (List<Mokinys>) object;
                System.out.println("elementų: " + list2.size());
                for (Mokinys a : list2) {
                    System.out.println(a);
                }
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();

        }

    }

    static void writeNode() {
        Node a = new Node("A");
        Node b = new Node("B");
        Node c = new Node("C");
        Node d = new Node("D");
        Node e = new Node("E");

        a.setLeft(b);
        a.setRight(c);

        b.setLeft(d);
        b.setRight(e);

        e.setRight(a);

        try (
                ObjectOutput ou = new ObjectOutputStream(
                        new BufferedOutputStream(
                                new FileOutputStream("C:/Users/Fractal/Documents/NetBeansProjects/JavaApplication/src/suFailais/sarasas.txt")))
        ) {
            ou.writeObject(a);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    static Node readNode() {
        try (
                ObjectInput in = new ObjectInputStream(
                        new BufferedInputStream(
                                new FileInputStream("C:/Users/Fractal/Documents/NetBeansProjects/JavaApplication/src/suFailais/sarasas.txt")))
        ) {

            Node node = (Node) in.readObject();
            return node;

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();

        }
        return null;
    }
}

class Node implements Serializable {

    String name;

    Node left;

    Node right;

    public Node(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return "Node{" +
                "name='" + name + '\'' +
                ", left=" + left +
                ", right=" + right +
                '}';
    }
}