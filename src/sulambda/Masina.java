package sulambda;

public class Masina {
    private String pavadinimas;
   private double suma;

    public Masina(String pavadinimas, double suma) {
        this.pavadinimas = pavadinimas;
        this.suma = suma;
    }

    public String getPavadinimas() {
        return pavadinimas;
    }

    public void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }

    public double getSuma() {
        return suma;
    }

    public void setSuma(double suma) {
        this.suma = suma;
    }

    @Override
    public String toString() {
        return "Masina{" +
                "pavadinimas='" + pavadinimas + '\'' +
                ", suma=" + suma +
                '}';
    }
}
