package sulambda;

public class Demo2 {

            public static void main(String[] args) {

                        Expression initial = Expression.of(10.0);
                      /* Operation o = new Operation() {
            @Override
           public double execute(double a) {
                                return a / 3;
                            }        };*/

                        Expression result = initial.plus(3).minus(4)
                                .operation(x -> Math.sqrt(x))
                                .operation(x -> x * x)
                                .operation(x->x/3);

                        System.out.println("Pradine: " + initial);
                System.out.println("Rezultatas: " + result);

                    }
}

        class Expression {

            double value;

            static Expression of(double value) {
                Expression e = new Expression();
                e.value = value;
                return e;
            }

            Expression plus(double x) {
                Expression e = new Expression();
                e.value = this.value + x;
                return e;
            }

           Expression minus(double x) {
                Expression e = new Expression();
                e.value = this.value - x;
                return e;    }

            Expression operation(Operation op) {
                Expression e = new Expression();
                e.value = op.execute(this.value);
                return e;
            }

           @Override   public String toString() {
                return String.valueOf(value);
            }
}

        @FunctionalInterface
interface Operation {
    double execute(double x);
}
