package klaseKlaseje;

import klaseKlaseje.Darbuotojas;
import mapas.Zmones;

import java.util.*;

public class main {

    public static void add(Map<String, List<Darbuotojas>> map, Darbuotojas e) {
        List<Darbuotojas> list = map.get(e.adress.city);//gauk value by key
        //tikrina ar yra masyvas(ka ssaugo), jei ne tai sukuria nauja tuscia masyva(ar tuscia ar iskart nauja reiksme iraso?)
        if (list == null) {
            list = new ArrayList<>();


        }

        list.add(e);

        //ka konkreciai sitas daro jei dar nesukurtas map
        map.put(e.adress.city, list);//ideda lista pagal key pirmaskey antras value
    }

    public static void main(String[] args) {



        Darbuotojas zmogus1 = new Darbuotojas("Balys", new Darbuotojas.Adresas("Jonava", "kazkokia 15"));
        Darbuotojas zmogus2 = new Darbuotojas("Antose", new Darbuotojas.Adresas("Vilnius", "kazkokia 99"));
        Darbuotojas zmogus3 = new Darbuotojas("Zose", new Darbuotojas.Adresas("Jonava", "kazkokia 20"));
        Darbuotojas zmogus4 = new Darbuotojas("Giedrius", new Darbuotojas.Adresas("Kaunas", "kazkokia 15"));


            Map<String, List<Darbuotojas>> map = new HashMap<>() ;


            add(map,zmogus1);
            add(map,zmogus2);
            add(map,zmogus3);
            add(map,zmogus4);



     //   System.out.println("Size of the map: "+ map.size());



            System.out.println( "Žmones yra is "+map.size() +" skirtingu miestu");


            // atspausdinti visus darbuotoju vardus kurie yra is Vilniaus
        System.out.println("Is Vilniaus: " );
            for(Darbuotojas e : map.get("Vilnius")){
                System.out.println(e.name);
            }

        // atspausdinti visus darbuotoju vardus kurie yra is Kauno
        System.out.println("Is Kauno: " );
        for(Darbuotojas e : map.get("Kaunas")){
            System.out.println(e.name);
        }

        // atspausdinti visus darbuotoju vardus kurie yra is Jonavos
        System.out.println("Is Jonavos: " );
        for(Darbuotojas e : map.get("Jonava")){
            System.out.println(e.name);
        }

        Set set = map.entrySet();

        Iterator iter = set.iterator();

        while(iter.hasNext()){
            Map.Entry<String, List<Darbuotojas>> mapEntry = (Map.Entry)iter.next();
            System.out.println(" key: " + mapEntry.getKey());

        }

        }
}