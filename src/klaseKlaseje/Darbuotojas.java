package klaseKlaseje;

public class Darbuotojas {

     String name;

     Adresas adress;

    public Darbuotojas(String name, Adresas adress) {
        this.name = name;
        this.adress = adress;
    }


    public static class Adresas{
        String city;
        String adress;

        public  Adresas(String city, String adress) {
           this.city=city;
           this.adress=adress;

        }
    }
}
