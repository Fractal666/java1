package penktadieniouzduotissuloginais;

import java.time.LocalDateTime;
import java.util.Date;

public class Log {
    private String adresas;
    private LocalDateTime data;
    private String linkas;

    public Log(String adresas, LocalDateTime data, String linkas) {
        this.adresas = adresas;
        this.data = data;
        this.linkas = linkas;
    }


    public String getAdresas() {
        return adresas;
    }

    public void setAdresas(String adresas) {
        this.adresas = adresas;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public String getLinkas() {
        return linkas;
    }

    public void setLinkas(String linkas) {
        this.linkas = linkas;
    }
}
