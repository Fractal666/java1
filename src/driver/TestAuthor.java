package driver;

/*
 * A test driver for the Author class.
 */
public class TestAuthor {
    public static void main(String[] args) {
        // Test constructor and toString()
        Author zmogus = new Author("ooo", "teck@nowhere.com", 'm');
        Author kitas = new Author("Tan Ah Teck", "teck@nowhere.com", 'm');
        System.out.println("opa----"+zmogus);  // toString()
        System.out.println("aaaaaaaaaa"+kitas.name);//kaip paimti varda be get o tiesiog kaip kitas.name;
        // Test Setters and Getters
        zmogus.setEmail("teck@somewhere.com");
        System.out.println(zmogus);  // toString()
        System.out.println("name is: " + zmogus.getName());
        System.out.println("gender is: " + zmogus.getGender());
        System.out.println("email is: " + zmogus.getEmail());
    }
}