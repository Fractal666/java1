package suDatom;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {


        List<Data> isklotine = new ArrayList<>();

        isklotine.add(new Data(LocalDate.of(2017, 10, 15), "Zigmas", new BigDecimal(100)));
        isklotine.add(new Data(LocalDate.of(2017, 11, 15), "Romas", new BigDecimal(100)));
        isklotine.add(new Data(LocalDate.of(2017, 12, 15), "Alfredas", new BigDecimal(100)));
        isklotine.add(new Data(LocalDate.of(2018, 1, 15), "Rita", new BigDecimal(200)));
        isklotine.add(new Data(LocalDate.of(2018, 2, 15), "Ruta", new BigDecimal(200)));
        isklotine.add(new Data(LocalDate.of(2018, 3, 15), "Algis", new BigDecimal(200)));
        isklotine.add(new Data(LocalDate.of(2018, 4, 15), "Regina", new BigDecimal(300)));
        isklotine.add(new Data(LocalDate.of(2018, 5, 15), "Petras", new BigDecimal(300)));
        isklotine.add(new Data(LocalDate.of(2018, 6, 15), "Saulius", new BigDecimal(300)));
        isklotine.add(new Data(LocalDate.of(2018, 7, 15), "Jadvyga", new BigDecimal(400)));
        isklotine.add(new Data(LocalDate.of(2018, 8, 15), "Angele", new BigDecimal(400)));
        isklotine.add(new Data(LocalDate.of(2018, 9, 15), "Viktoras", new BigDecimal(400)));
        isklotine.add(new Data(LocalDate.of(2018, 10, 15), "Zigmas", new BigDecimal(100)));
        isklotine.add(new Data(LocalDate.of(2018, 11, 15), "Romas", new BigDecimal(100)));
        isklotine.add(new Data(LocalDate.of(2018, 12, 15), "Alfredas", new BigDecimal(100)));


        for (Data x : isklotine) {
            System.out.println(x);
        }

        System.out.println("-----------------------------------------------------------------------------");


//jei to map tai yra kei value ir tada trecia reiksme ka daryti jei bus dublikuotas key, o grouping by tiesiog key ir value

        Map<Integer, Map<Integer, BigDecimal>> kazkas= isklotine.stream().collect(Collectors.groupingBy(x->x.getData().getYear(), Collectors.toMap(x->
                x.getData().getMonth().getValue()<4?1:
                        x.getData().getMonth().getValue()<7?2:
                                x.getData().getMonth().getValue()<10?3:4, y->y.getSuma(), (y1,y2)->y1.add(y2))));



        System.out.println(kazkas);

    }}
