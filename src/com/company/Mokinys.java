package com.company;

import java.util.stream.IntStream;

public class Mokinys {

    String vardas;
    String pavarde;
    int klase;
    static int[] mokiniuKlases =new int[12];
    int[]masyvas;




    Mokinys(String vardas, String pavarde, int klase, int[] masyvas) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.klase = klase;
        mokiniuKlases[klase]++;
        this.masyvas=masyvas;
    }

    public int getKlase(){
        return this.klase;
    }
    public String getPavarde(){
        return this.pavarde;
    }
    public String getVardas(){
        return this.vardas;
    }
    public int[] getMasyva(){
        return this.masyvas;
    }


    public double average(){

        return  (double) IntStream.of(this.masyvas).sum()/masyvas.length;
    }


    public String toString() {
        return vardas + pavarde+ klase + masyvas;
    }
}



