package mapas;

import turnyras.Zmogus;

import java.util.*;

public class main {

    public static void main(String[] args) {


        Zmones zmogus1 = new Zmones("Giedrius", "Žilas", 111111);
        Zmones zmogus2 = new Zmones("Rasa", "Pelėda", 22222222);
        Zmones zmogus3 = new Zmones("yyy", "Pey", 33333333);
        Zmones zmogus4 = new Zmones("Dar", "adsasd", 111111);

        HashMap<Integer, Zmones> hmap = new HashMap<Integer, Zmones>();

        hmap.put(zmogus1.getAsmensKodas(), zmogus1);
        hmap.put(zmogus2.getAsmensKodas(), zmogus2);
        hmap.put(zmogus3.getAsmensKodas(), zmogus3);
        hmap.put(zmogus4.getAsmensKodas(), zmogus4);


        Set set = hmap.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
           // System.out.print("key is: "+ mentry.getKey() + " & Value is: ");
            System.out.println(mentry.getValue());
        }
        Map<Integer, Zmones> map = new TreeMap<Integer, Zmones>(hmap);
        System.out.println("After Sorting:");
        Set set2 = map.entrySet();
        Iterator iterator2 = set2.iterator();
        while(iterator2.hasNext()) {
            Map.Entry me2 = (Map.Entry)iterator2.next();
            System.out.println(me2.getValue());
        }

    }




}
